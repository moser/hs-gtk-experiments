-- | WebKit browser demo.
--  Author      :  Andy Stewart
--  Copyright   :  (c) 2010 Andy Stewart <lazycat.manatee@gmail.com>

-- | This simple browser base on WebKit API.
-- For simple, i just make all link open in current window.
-- Of course, you can integrate signal `createWebView` with `notebook`
-- to build multi-tab browser.
--
-- You can click right-button for forward or backward page.
--
-- Usage:
--      webkit [uri]
--
module Webkit where

import           Control.Monad.Trans

import           Data.Text
import           Data.Maybe

import           Graphics.UI.Gtk
import           Graphics.UI.Gtk.WebKit.WebFrame
import           Graphics.UI.Gtk.WebKit.WebView
import           Graphics.UI.Gtk.WebKit.WebDataSource
import           Graphics.UI.Gtk.WebKit.DOM.Document
import           Graphics.UI.Gtk.WebKit.DOM.HTMLElement


import           System.Environment
import           System.Process


-- | Main entry.
main :: IO ()
main = editor


-- | Necessary to clarify types
webFrameGetTitle' :: WebFrame -> IO (Maybe String)
webFrameGetTitle' = webFrameGetTitle


-- | Get the current HTML of an WebView
getHtml :: WebView -> IO (Maybe String)
getHtml webView = do
    webViewExecuteScript webView "document.title='';"
    webViewExecuteScript webView "document.title=document.documentElement.innerHTML;"
    frame <- webViewGetMainFrame webView
    webFrameGetTitle' frame


editor :: IO ()
editor = do
  -- Init.
  initGUI

  -- Create window.
  window <- windowNew
  windowSetDefaultSize window 900 600
  windowSetPosition window WinPosCenter
  set window [windowOpacity := 0.8]   -- this function need window-manager support Alpha channel in X11

  -- Buttons
  boldBtn <- buttonNew
  set boldBtn [ buttonLabel := "Bold" ]
  showBtn <- buttonNew
  set showBtn [ buttonLabel := "Show" ]
  quitBtn <- buttonNew
  set quitBtn [ buttonLabel := "Quit" ]

  -- Create WebKit view.
  webView <- webViewNew
  webViewSetEditable webView True

  boldBtn `on` buttonActivated $
    webViewExecuteScript webView "document.execCommand('bold', false, false)"

  showBtn `on` buttonActivated $ do
    html <- getHtml webView
    putStrLn $ case html of 
      Just htmlString -> htmlString
      _ -> "Something went wrong"

  quitBtn  `on` buttonActivated $ do
    widgetDestroy window
    mainQuit

  -- Create window box.
  winBox <- vBoxNew False 0

  -- Create scroll window.
  scrollWin <- scrolledWindowNew Nothing Nothing

  -- Load HTML
  webViewLoadHtmlString webView
     "<html><body><h1>Title</h1>Text <b>body</b>...</body></html>"
     "file:///"

  -- Connect and show.
  boxPackStart winBox boldBtn PackNatural 0
  boxPackStart winBox showBtn PackNatural 0
  boxPackStart winBox quitBtn PackNatural 0
  boxPackStart winBox scrollWin PackGrow 0
  window `containerAdd` winBox
  scrollWin `containerAdd` webView
  window `on` deleteEvent $ lift mainQuit >> return True
  widgetShowAll window

  mainGUI
